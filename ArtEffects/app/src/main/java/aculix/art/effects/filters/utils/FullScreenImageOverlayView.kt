package aculix.art.effects.filters.utils

import aculix.art.effects.filters.R
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.view_full_screen_image_overlay.view.*
import java.io.File

class FullScreenImageOverlayView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var mListener: FullScreenImageOverlayActions? = null

    init {
        View.inflate(context, R.layout.view_full_screen_image_overlay, this)
        setBackgroundColor(Color.TRANSPARENT)
    }

    fun update(imageFile: File, itemPos: Int) {
        ivShareFullScreenImageOverlay.setOnClickListener { mListener?.onShareClick(imageFile.absolutePath) }
        ivDeleteFullScreenImageOverlay.setOnClickListener { mListener?.onDeleteClick(imageFile.absolutePath, itemPos, imageFile) }
    }

    fun setActionListener(listener: FullScreenImageOverlayActions) {
        mListener = listener
    }

    interface FullScreenImageOverlayActions {
        fun onShareClick(imagePath: String)
        fun onDeleteClick(imagePath: String, itemPos: Int, imageFile: File)
    }
}