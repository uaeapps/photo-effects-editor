package aculix.art.effects.filters.fragment


import aculix.art.effects.filters.BuildConfig
import aculix.art.effects.filters.R
import aculix.art.effects.filters.adapter.EffectsAdapter
import aculix.art.effects.filters.model.Effect
import aculix.art.effects.filters.utils.SingleMediaScanner
import aculix.art.effects.filters.utils.startShareImageIntent
import aculix.core.base.BaseFragment
import aculix.core.extensions.*
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import id.zelory.compressor.Compressor
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import kotlinx.android.synthetic.main.fragment_effects.*
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.io.File
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.MotionEvent
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import com.afollestad.materialdialogs.customview.customView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.yalantis.ucrop.UCrop
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class EffectsFragment : BaseFragment(), EffectsAdapter.EffectsAdapterActions {

    private var selectedImage: Image? = null // Image selected using the ImagePicker
    private lateinit var compressedImage: File // Image compressed by the Compressor
    private var resultBitmap: Bitmap? = null
    private lateinit var loadingDialog: MaterialDialog
    private lateinit var effectsList: ArrayList<Effect>
    private var effectsAdapter: EffectsAdapter? = null
    private lateinit var effectName: String
    private lateinit var downloadInterstitialAd: InterstitialAd

    private lateinit var inferenceInterface: TensorFlowInferenceInterface
    private val MODEL_FILE = "file:///android_asset/stylize_quantized.pb"
    private val INPUT_NODE = "input"
    private val OUTPUT_NODE = "transformer/expand/conv3/conv/Sigmoid"
    private val STYLE_NODE = "style_num"
    private val NUM_STYLES = 26
    private lateinit var styleVals: FloatArray
    private val outputImageSize = 1080

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_effects, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbarMenu()
        startImagePicker()
        inferenceInterface = TensorFlowInferenceInterface(context?.assets, MODEL_FILE)

        loadBannerAd()
        initializeInterstitialAd()
        loadInterstitialAd()

        // Main image long press
        ivEffects.setOnTouchListener { v, event ->
            resultBitmap?.let {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        ivEffects.loadImageFromPath(compressedImage.absolutePath)
                    }
                    MotionEvent.ACTION_UP -> {
                        ivEffects.setImageBitmap(it)
                    }
                }
            }
            true
        }

        // Share onClick
        btnShareEffects.setOnClickListener {
            val name = "${effectName}_${selectedImage!!.name}"
            val uri = downloadImage(name, resultBitmap, EffectsOutput.SHARE, true)
            uri?.let { it1 -> startShareImageIntent(context, it1) }
        }

        // Download onClick
        btnDownloadEffects.setOnClickListener {
            val name = "${effectName}_${selectedImage!!.name}"
            downloadImage(name, resultBitmap, EffectsOutput.DOWNLOAD, false)
            if (downloadInterstitialAd.isLoaded) downloadInterstitialAd.show()
        }
    }

    override fun onEffectSelected(effectPosition: Int, name: String) {
        effectName = name.toLowerCase().replace(" ", "_")
        styleVals = FloatArray(NUM_STYLES)
        styleVals[effectPosition] = 1.0f
        stylizeImage(getBitmap(compressedImage.absolutePath))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            groupEmptyStateEffects.makeGone() // Hide the empty state
            groupButtonEffects.makeGone() // Hide the buttons

            selectedImage = ImagePicker.getFirstImageOrNull(data)
            selectedImage?.let { startImageCropper(Uri.fromFile(File(it.path))) } // Starts the Image Cropper

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            val croppedFile = File(UCrop.getOutput(data!!)!!.path)
            compressedImage = compressImage(croppedFile) // Compress the cropped image
            ivEffects.loadImageFromPath(compressedImage.absolutePath) // Show image in ImageView
            if (effectsAdapter == null) initializeEffectsRecyclerView()
        } else {
            if (selectedImage == null) groupEmptyStateEffects.makeVisible() // Show only if no image was selected previously
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun startImagePicker() {
        ImagePicker.create(this)
            .folderMode(false)
            .returnMode(ReturnMode.ALL)
            .toolbarFolderTitle(getString(R.string.text_select_image))
            .toolbarImageTitle(getString(R.string.text_tap_to_select))
            .single()
            .start()
    }

    private fun startImageCropper(sourceUri: Uri) {
        val destUri = Uri.fromFile(
            File(
                context?.filesDir,
                "${UUID.randomUUID()}_${selectedImage?.name}"
            )
        ) // Save the cropped image in the private directory

        val uCropOptions = UCrop.Options().apply {
            setToolbarTitle(getString(R.string.text_crop_photo))
            setToolbarColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            setStatusBarColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            setToolbarWidgetColor(ContextCompat.getColor(context!!, R.color.whiteColor))
            setCompressionQuality(60)
            setCompressionFormat(Bitmap.CompressFormat.JPEG)

        }

        UCrop.of(sourceUri, destUri)
            .withAspectRatio(1f, 1f)
            .withOptions(uCropOptions)
            .start(context!!, this)
    }

    private fun compressImage(fileToCompress: File): File {
        return Compressor(context)
            .setCompressFormat(Bitmap.CompressFormat.JPEG)
            .setDestinationDirectoryPath(context?.filesDir?.absolutePath) // Save the compressed image in the private directory
            .compressToFile(fileToCompress, "${UUID.randomUUID()}_${fileToCompress.name}")
    }

    private fun showLoadingDialog() {
        loadingDialog = MaterialDialog(context!!).apply {
            customView(R.layout.dialog_loader)
            cancelOnTouchOutside(false)
            noAutoDismiss()
        }
        loadingDialog.show()
    }

    private fun setupToolbarMenu() {
        // Setup Toolbar
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        toolbarEffects.setupWithNavController(navController, appBarConfiguration)

        // Inflate menu
        toolbarEffects.inflateMenu(R.menu.menu_effects)
        toolbarEffects.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.miAddEffects -> {
                    startImagePicker()
                }
            }
            true
        }
    }

    private fun initializeEffectsList() {
        effectsList = ArrayList()

        val style0 = Effect(
            getString(R.string.text_style0),
            R.drawable.style0
        )

        val style1 = Effect(
            getString(R.string.text_style1),
            R.drawable.style1
        )


        val style2 = Effect(
            getString(R.string.text_style2),
            R.drawable.style2
        )

        val style3 = Effect(
            getString(R.string.text_style3),
            R.drawable.style3
        )

        val style4 = Effect(
            getString(R.string.text_style4),
            R.drawable.style4
        )

        val style5 = Effect(
            getString(R.string.text_style5),
            R.drawable.style5
        )

        val style6 = Effect(
            getString(R.string.text_style6),
            R.drawable.style6
        )

        val style7 = Effect(
            getString(R.string.text_style7),
            R.drawable.style7
        )

        val style8 = Effect(
            getString(R.string.text_style8),
            R.drawable.style8
        )

        val style9 = Effect(
            getString(R.string.text_style9),
            R.drawable.style9
        )

        val style10 = Effect(
            getString(R.string.text_style10),
            R.drawable.style10
        )

        val style11 = Effect(
            getString(R.string.text_style11),
            R.drawable.style11
        )

        val style12 = Effect(
            getString(R.string.text_style12),
            R.drawable.style12
        )

        val style13 = Effect(
            getString(R.string.text_style13),
            R.drawable.style13
        )

        val style14 = Effect(
            getString(R.string.text_style14),
            R.drawable.style14
        )

        val style15 = Effect(
            getString(R.string.text_style15),
            R.drawable.style15
        )

        val style16 = Effect(
            getString(R.string.text_style16),
            R.drawable.style16
        )

        val style17 = Effect(
            getString(R.string.text_style17),
            R.drawable.style17
        )

        val style18 = Effect(
            getString(R.string.text_style18),
            R.drawable.style18
        )

        val style19 = Effect(
            getString(R.string.text_style19),
            R.drawable.style19
        )

        val style20 = Effect(
            getString(R.string.text_style20),
            R.drawable.style20
        )

        val style21 = Effect(
            getString(R.string.text_style21),
            R.drawable.style21
        )

        val style22 = Effect(
            getString(R.string.text_style22),
            R.drawable.style22
        )

        val style23 = Effect(
            getString(R.string.text_style23),
            R.drawable.style23
        )

        val style24 = Effect(
            getString(R.string.text_style24),
            R.drawable.style24
        )

        val style25 = Effect(
            getString(R.string.text_style25),
            R.drawable.style25
        )

        effectsList.apply {
            add(style0)
            add(style1)
            add(style2)
            add(style3)
            add(style4)
            add(style5)
            add(style6)
            add(style7)
            add(style8)
            add(style9)
            add(style10)
            add(style11)
            add(style12)
            add(style13)
            add(style14)
            add(style15)
            add(style16)
            add(style17)
            add(style18)
            add(style19)
            add(style20)
            add(style21)
            add(style22)
            add(style23)
            add(style24)
            add(style25)
        }
    }

    private fun initializeEffectsRecyclerView() {
        initializeEffectsList()
        rvEffects.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        effectsAdapter = EffectsAdapter(effectsList, this, context)
        rvEffects.adapter = effectsAdapter
    }

    private fun getBitmap(filePath: String): Bitmap {
        var scaledPhoto: Bitmap? = null

        val bitmap = BitmapFactory.decodeFile(filePath)
        scaledPhoto = Bitmap.createScaledBitmap(bitmap, outputImageSize, outputImageSize, false)

        return scaledPhoto
    }

    /**
     * Applies the style transfer using the TensorFlow model
     */
    private fun stylizeImage(bitmap: Bitmap) {
        showLoadingDialog() // Show the loading dialog

        lifecycleScope.launch {
            try {
                withContext(Dispatchers.Default) {
                    val intValues = IntArray(bitmap.width * bitmap.height)
                    val floatValues = FloatArray(bitmap.width * bitmap.height * 3)

                    bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

                    for (i in 0 until intValues.size) {
                        val value = intValues[i]
                        floatValues[i * 3] = ((value shr 16) and 0xFF) / 255.0f
                        floatValues[i * 3 + 1] = ((value shr 8) and 0xFF) / 255.0f
                        floatValues[i * 3 + 2] = (value and 0xFF) / 255.0f
                    }
                    // Copy the input data into TensorFlow.
                    inferenceInterface.feed(
                        INPUT_NODE,
                        floatValues,
                        1,
                        bitmap.width.toLong(),
                        bitmap.height.toLong(),
                        3
                    )
                    inferenceInterface.feed(STYLE_NODE, styleVals, NUM_STYLES.toLong())

                    // Execute the output node's dependency sub-graph.
                    inferenceInterface.run(arrayOf(OUTPUT_NODE), false)

                    // Copy the data from TensorFlow back into our array.
                    inferenceInterface.fetch(OUTPUT_NODE, floatValues)

                    for (i in 0 until intValues.size) {
                        intValues[i] = (0xFF000000.toInt()
                                or ((floatValues[i * 3] * 255).toInt() shl 16)
                                or ((floatValues[i * 3 + 1] * 255).toInt() shl 8)
                                or (floatValues[i * 3 + 2] * 255).toInt())
                    }

                    resultBitmap = Bitmap.createBitmap(intValues, bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
                }

                // Process the result
                loadingDialog.dismiss() // Dismiss the loading dialog
                ivEffects.setImageBitmap(resultBitmap)
                groupButtonEffects.makeVisible()

            } catch (e: Exception) {
                loadingDialog.dismiss() // Dismiss the loading dialog
                clEffects.showSnackbar(R.string.error_apply_effect) {}
            }

        }


    }

    private fun downloadImage(
        fileName: String,
        bitmap: Bitmap?,
        effectsOutput: EffectsOutput,
        uriReturn: Boolean
    ): Uri? {
        val state = Environment.getExternalStorageState()
        var bmpUri: Uri? = null

        if (Environment.MEDIA_MOUNTED == state) {
            // Get the directory for the user's public pictures directory.
            val downloadDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                context?.getString(R.string.app_name)
            )

            if (!downloadDir.isDirectory) {
                //Creates directory named by this file
                downloadDir.mkdirs()
            }

            val file = File(downloadDir, fileName)
            var ostream: FileOutputStream? = null
            try {
                ostream = FileOutputStream(file)
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                ostream.flush()
                ostream.close()

                when (effectsOutput) {
                    EffectsOutput.SHARE -> {
                    }
                    EffectsOutput.DOWNLOAD -> {
                        context?.toast(getString(R.string.text_download_successful))
                        context?.let { SingleMediaScanner(it, file) }
                    }
                }

                if (uriReturn) {
                    bmpUri = context?.let {
                        FileProvider.getUriForFile(
                            it,
                            "${BuildConfig.APPLICATION_ID}.fileprovider",
                            file
                        )
                    }
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return bmpUri
    }

    private fun loadBannerAd() {
        val adRequest = AdRequest.Builder().build()
        bannerAdEffects.loadAd(adRequest)
    }

    private fun initializeInterstitialAd() {
        downloadInterstitialAd = InterstitialAd(context)
        downloadInterstitialAd.adUnitId = getString(R.string.effects_download_interstitial_ad_id)

        // Reload ad once shown
        downloadInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                loadInterstitialAd()
            }
        }
    }

    private fun loadInterstitialAd() {
        downloadInterstitialAd.loadAd(AdRequest.Builder().build())
    }


    /**
     * Enum class to determine what is to be done once the file is downloaded
     */
    enum class EffectsOutput {
        DOWNLOAD, SHARE
    }
}
