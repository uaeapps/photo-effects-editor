package aculix.art.effects.filters.model

import androidx.annotation.DrawableRes

data class Effect(val name: String,
                  @DrawableRes val drawableId: Int)