package aculix.art.effects.filters.adapter

import aculix.art.effects.filters.R
import aculix.art.effects.filters.model.Effect
import aculix.core.extensions.inflate
import aculix.core.extensions.loadImageFromDrawable
import aculix.core.extensions.makeVisible
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_effects.view.*

class EffectsAdapter(
    private var effectsList: List<Effect>,
    listener: EffectsAdapterActions,
    val context: Context?
)
    : RecyclerView.Adapter<EffectsAdapter.EffectsViewHolder>() {

    private var mListener = listener
    private var lastCheckedPos = -1

    class EffectsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var cardView = view.cvEffectsListRow
        var overlay = view.ivOverlayEffectsList
        var effectName = view.tvNameEffectsListRow
        var effectPhoto = view.ivEffectsListRow
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        EffectsViewHolder(
            parent.inflate(
                R.layout.item_effects
            )
        )

    override fun onBindViewHolder(holder: EffectsViewHolder, position: Int) {

        val effect = effectsList[position]

        holder.effectName.text = effect.name
        holder.effectPhoto.loadImageFromDrawable(effect.drawableId)

        holder.overlay.visibility = if (position == lastCheckedPos) View.VISIBLE else View.GONE

        holder.cardView.setOnClickListener {
            // Check only single CardView at a time
            holder.overlay.makeVisible()
            if (lastCheckedPos != position) {
                notifyItemChanged(lastCheckedPos)
                lastCheckedPos = position
            }
            mListener.onEffectSelected(position, effect.name)
        }

    }

    override fun getItemCount(): Int {
        return effectsList.size
    }

    interface EffectsAdapterActions {

        fun onEffectSelected(effectPosition: Int, name: String)

    }

}