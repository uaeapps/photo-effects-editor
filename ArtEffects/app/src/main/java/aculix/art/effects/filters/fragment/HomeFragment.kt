package aculix.art.effects.filters.fragment

import aculix.art.effects.filters.ArtEffects
import aculix.art.effects.filters.R
import aculix.art.effects.filters.adapter.HomeAdapter
import aculix.art.effects.filters.utils.FullScreenImageOverlayView
import aculix.art.effects.filters.utils.SingleMediaScanner
import aculix.art.effects.filters.utils.startShareImageIntent
import aculix.core.base.BaseFragment
import aculix.core.extensions.*
import android.Manifest
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.checkbox.checkBoxPrompt
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

class HomeFragment : BaseFragment(), EasyPermissions.PermissionCallbacks, HomeAdapter.HomeAdapterActions {

    private var fabMainActivity: FloatingActionButton? = null
    private var bottomAppBar: BottomAppBar? = null
    private var storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var requestCodeStorage = 345
    private lateinit var imagesList: ArrayList<File>
    private lateinit var homeAdapter: HomeAdapter
    private lateinit var stfalconImageViewer: StfalconImageViewer<File>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViews()
        showViews()

        // Check for storage permission
        if (EasyPermissions.hasPermissions(context!!, storagePermission)) {
            getEditHistory()
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.text_storage_permission_rationale), requestCodeStorage, storagePermission)
        }

        // fab onClick
        fabMainActivity?.setOnClickListener {
            hideViews()
            findNavController().navigate(R.id.action_homeFragment_to_effectsFragment)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        clHome.showSnackbar(R.string.text_storage_permission_denied_home) {
            action(R.string.text_allow) {
                EasyPermissions.requestPermissions(this@HomeFragment, getString(R.string.text_storage_permission_rationale), requestCodeStorage, storagePermission)
            }
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        getEditHistory()
    }

    override fun onImageClicked(imageView: AppCompatImageView, imageFile: File, itemPos: Int) {
        val overlayView = setupOverlayView(imageFile, itemPos)

        // Show the full screen Stfalcon ImageViewer
        stfalconImageViewer = StfalconImageViewer.Builder<File>(context, imagesList) { view, file ->
            view.loadImageFromPath(file.absolutePath)
        }
            .withStartPosition(itemPos)
            //.withTransitionFrom(imageView)
            .withImagesMargin(context, R.dimen.stfalcon_image_margin)
            .withHiddenStatusBar(true)
            .withOverlayView(overlayView)
            .withImageChangeListener { position ->
                overlayView.update(
                    imagesList[position],
                    position
                )
            }
            .show()
    }

    override fun onShareClicked(imagePath: String) {
        startShareImageIntent(context, imagePath.toUri())
    }

    override fun onDeleteClicked(imagePath: String, itemPos: Int, imageFile: File) {
        if (ArtEffects.showDeleteDialog) showDeleteDialog(imagePath, itemPos, false, imageFile) else deleteFile(imagePath, itemPos, false, imageFile)
    }

    private fun initializeViews() {
        fabMainActivity = activity?.findViewById(R.id.fabMainActivity)
        bottomAppBar = activity?.findViewById(R.id.bottomAppBar)
    }

    private fun showViews() {
        fabMainActivity?.makeVisible()
        bottomAppBar?.makeVisible()
    }

    private fun hideViews() {
        fabMainActivity?.makeGone()
        bottomAppBar?.makeGone()
    }

    private fun getEditHistory() {
        imagesList = ArrayList()
        lifecycleScope.launch {
            val request = withContext(Dispatchers.IO) {
                async {
                    val imagesDir = File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        context?.getString(R.string.app_name)
                    )
                    val filesList = imagesDir.listFiles()?.sortedByDescending { it.lastModified() }

                    if (filesList != null) {
                        for (f in filesList) {
                            imagesList.add(f)
                        }
                    }
                }
            }
            setupRecyclerView(request.await(), imagesList)
        }
    }

    private fun setupRecyclerView(await: Unit, imagesList: ArrayList<File>) {
        showEmptyState(imagesList)
        rvHome.layoutManager = GridLayoutManager(context, 2)
        homeAdapter = HomeAdapter(imagesList, this, context)
        rvHome.adapter = homeAdapter
    }

    private fun showEmptyState(imagesList: ArrayList<File>) {
        if (imagesList.size <= 0) groupEmptyStateHome.makeVisible() else groupEmptyStateHome.makeGone()
    }

    /**
     * Sets up the overlay view to be shown over the image in the
     * Stfalcon ImageViewer
     */
    private fun setupOverlayView(imageFile: File, itemPos: Int): FullScreenImageOverlayView {
        // Initialize the overlay view
        val overlayView = FullScreenImageOverlayView(context).apply {
            update(imageFile, itemPos)
        }

        // Setting up listener to listen events from overlay view
        overlayView.setActionListener(object : FullScreenImageOverlayView.FullScreenImageOverlayActions {

            override fun onShareClick(imagePath: String) {
                startShareImageIntent(context, imagePath.toUri())
            }

            override fun onDeleteClick(imagePath: String, itemPos: Int, imageFile: File) {
                if (ArtEffects.showDeleteDialog) showDeleteDialog(imagePath, itemPos, true, imageFile) else deleteFile(
                    imagePath,
                    itemPos,
                    true,
                    imageFile
                )
            }
        })
        return overlayView
    }

    private fun showDeleteDialog(
        imagePath: String,
        itemPos: Int,
        isRequestFromStfalcon: Boolean,
        imageFile: File
    ) {
        MaterialDialog(context!!).show {
            message(R.string.dialog_delete_msg)
            checkBoxPrompt(R.string.cb_dont_ask_delete) { checked -> if (checked) ArtEffects.showDeleteDialog = false  }
            negativeButton(R.string.btn_no) { dismiss() }
            positiveButton(R.string.btn_yes) { deleteFile(imagePath, itemPos, isRequestFromStfalcon, imageFile) }
        }
    }

    private fun deleteFile(
        imagePath: String,
        itemPos: Int,
        isRequestFromStfalcon: Boolean,
        imageFile: File
    ) {
        val file = File(imagePath)
        if (file.exists()) {
            file.delete()
            imagesList.removeAt(itemPos) // Remove image from original image list
            homeAdapter.onItemDeleted(itemPos) // Remove image from copied list in adapter
            showEmptyState(imagesList)
            if (isRequestFromStfalcon) stfalconImageViewer.updateImages(imagesList) // Inform stfalcon about change in images list
            SingleMediaScanner(context!!, imageFile) // Send broadcast of file deletion
        } else {
            clHome.showSnackbar(R.string.error_delete) {}
        }
    }
}
