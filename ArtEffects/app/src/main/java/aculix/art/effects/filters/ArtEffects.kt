package aculix.art.effects.filters

import android.app.Application
import com.google.android.gms.ads.MobileAds

class ArtEffects : Application() {

    companion object {
        var showDeleteDialog = true
    }

    override fun onCreate() {
        super.onCreate()

        // Initialize Admob
        MobileAds.initialize(this, getString(R.string.admob_app_id))
    }
}