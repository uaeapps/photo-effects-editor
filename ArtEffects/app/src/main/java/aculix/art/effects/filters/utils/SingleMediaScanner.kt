package aculix.art.effects.filters.utils

import android.content.Context
import android.media.MediaScannerConnection
import android.net.Uri
import java.io.File

/**
 * Sends broadcast to the media scanner about the creation of the new file
 */
class SingleMediaScanner(context: Context, private val mFile: File) :
    MediaScannerConnection.MediaScannerConnectionClient {

    private val mMs: MediaScannerConnection = MediaScannerConnection(context, this)

    init {
        mMs.connect()
    }

    override fun onMediaScannerConnected() {
        mMs.scanFile(mFile.absolutePath, null)
    }

    override fun onScanCompleted(path: String, uri: Uri) {
        mMs.disconnect()
    }

}