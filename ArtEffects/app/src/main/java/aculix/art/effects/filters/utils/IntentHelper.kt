package aculix.art.effects.filters.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

fun startShareImageIntent(context: Context?, uri: Uri) {
    val shareIntent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_STREAM, uri)
        type = "image/*"
    }
    // Launch sharing dialog for image
    context?.startActivity(Intent.createChooser(shareIntent, "Share Image"))
}