package aculix.art.effects.filters.adapter

import aculix.art.effects.filters.R
import aculix.core.extensions.inflate
import aculix.core.extensions.loadImageFromPath
import aculix.core.extensions.toast
import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_home.view.*
import java.io.File

class HomeAdapter(
    private var imagesList: List<File>,
    listener: HomeAdapterActions,
    val context: Context?
)
    : RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    private var mImagesList = ArrayList(imagesList)
    private var mListener = listener
    private val noOfColumns = 2
    private val displayMetrics = context?.resources?.displayMetrics
    private val columnWidth = displayMetrics!!.widthPixels / noOfColumns

    class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mainImage = view.ivHomeItem
        val shareImage = view.ivShareHomeItem
        val deleteImage = view.ivDeleteHomeItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HomeViewHolder(
            parent.inflate(
                R.layout.item_home
            )
        )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {

        val image = mImagesList[position]

        with(holder) {
            mainImage.apply {
                layoutParams.height = columnWidth - 16
                layoutParams.width = columnWidth
                loadImageFromPath(image.absolutePath)
            }

            // Image onClick
            mainImage.setOnClickListener {
                mListener.onImageClicked(mainImage, image, position)
            }

            // Share onClick
            shareImage.setOnClickListener {
                mListener.onShareClicked(image.absolutePath)
            }

            // Delete onClick
            deleteImage.setOnClickListener {
                mListener.onDeleteClicked(image.absolutePath, position, image)
            }
        }

    }

    override fun getItemCount(): Int {
        return mImagesList.size
    }

    fun onItemDeleted(position: Int) {
        mImagesList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, mImagesList.size)
    }


    interface HomeAdapterActions {
        fun onImageClicked(imageView: AppCompatImageView, imageFile: File, itemPos: Int)

        fun onShareClicked(imagePath: String)

        fun onDeleteClicked(imagePath: String, itemPos: Int, imageFile: File)
    }

}