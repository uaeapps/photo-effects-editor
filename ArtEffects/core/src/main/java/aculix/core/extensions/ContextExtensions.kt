package aculix.core.extensions


import aculix.core.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import saschpe.android.customtabs.CustomTabsHelper
import saschpe.android.customtabs.WebViewFallback


/**
 * Relaunches the current activity
 */
fun Context.relaunchActivity(context: Activity, intent: Intent) {
    context.finish()
    startActivity(intent)
}

/**
 * Shows the toast message
 */
fun Context.toast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

/**
 * Opens the passed URL in the Chrome Custom Tabs
 */
fun Context.openUrl(url: String) {
    val customTabsIntent = CustomTabsIntent.Builder()
        .addDefaultShareMenuItem()
        .setToolbarColor(Color.WHITE)
        .setShowTitle(true)
        .build()

    // This is optional but recommended
    CustomTabsHelper.addKeepAliveExtra(this, customTabsIntent.intent)

    CustomTabsHelper.openCustomTab(
        this,
        customTabsIntent,
        Uri.parse(url),
        WebViewFallback() // Opens in system browser if Chrome isn't installed on device
    )
}

fun Context.openAppInGooglePlay() {
    val appPackageName = "aculix.artistic.deep.filters"

    try {
        // Try to open in the Google Play app
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
    } catch (exception: android.content.ActivityNotFoundException) {
        // Google Play app is not installed. Open URL in the browser.
        openUrl("https://play.google.com/store/apps/details?id=$appPackageName")
    }
}
