package aculix.core.extensions

import aculix.core.R
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import java.io.File

// Loads the Image from URL using Glide with thumbnail
fun ImageView.loadUrlWithThumbnail(url: String?) {
    Glide.with(context).load(url).apply(RequestOptions().placeholder(R.drawable.ic_place_holder_wrapper))
        .thumbnail(0.1f).into(this)
}

// Loads the Image from URL using Glide without thumbnail
fun ImageView.loadUrl(url: String?) {
    Glide.with(context)
        .load(url)
        .apply(RequestOptions().placeholder(R.drawable.ic_place_holder_wrapper))
        .into(this)
}

/**
 * Loads the Image from the local path and also returns the width and height
 * of the image
 */
fun ImageView.loadImageFromPath(imagePath: String) {
    Glide.with(context)
        .load(Uri.fromFile(File(imagePath)))
        .apply(RequestOptions().placeholder(R.drawable.ic_place_holder_wrapper))
        .into(this)
}

/**
 * Loads the image from the byte array
 */
fun ImageView.loadByteArray(byteArray: ByteArray?) {
    Glide.with(context)
        .load(byteArray)
        .into(this)
}

/**
 * Loads the Image from the local path
 */
fun ImageView.loadImageFromDrawable(drawableId: Int) {
    Glide.with(context)
        .load(drawableId)
        .apply(RequestOptions().placeholder(R.drawable.ic_place_holder_wrapper))
        .into(this)
}
